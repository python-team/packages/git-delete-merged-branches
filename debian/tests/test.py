import git_delete_merged_branches.tests
import os
import re
import unittest

tests = git_delete_merged_branches.tests.__path__[0]
names = [f"git_delete_merged_branches.tests.{match.group(1)}"
         for match in [re.search("(test_.*)\.py", filename)
                       for filename in os.listdir(tests)] if match]
suite = unittest.defaultTestLoader.loadTestsFromNames(names)
result = unittest.TextTestRunner().run(suite)

exit(len(result.errors) + len(result.failures))
